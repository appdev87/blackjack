

package ch.hectorcode.blackjack;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String[] cards = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

    private TextView[] p1TextView = new TextView[6];
    private TextView[] p2TextView = new TextView[6];
    private int p1Counter = 0, p1Score = 0;
    private int p2Counter = 0, p2Score = 0;
    private Button hitBtn, stayBtn, foldBtn;

    private TextView winnerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p1TextView[0] = findViewById(R.id.textView8);
        p1TextView[1] = findViewById(R.id.textView9);
        p1TextView[2] = findViewById(R.id.textView10);
        p1TextView[3] = findViewById(R.id.textView11);
        p1TextView[4] = findViewById(R.id.textView12);
        p1TextView[5] = findViewById(R.id.textView13);

        p2TextView[0] = findViewById(R.id.textView2);
        p2TextView[1] = findViewById(R.id.textView3);
        p2TextView[2] = findViewById(R.id.textView4);
        p2TextView[3] = findViewById(R.id.textView5);
        p2TextView[4] = findViewById(R.id.textView6);
        p2TextView[5] = findViewById(R.id.textView7);


        winnerView = findViewById(R.id.winnerView);

        hitBtn = findViewById(R.id.hitBtn);
        hitBtn.setOnClickListener(this);

        stayBtn = findViewById(R.id.stayBtn);
        stayBtn.setOnClickListener(this);

        foldBtn = findViewById(R.id.foldBtn);
        foldBtn.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        Log.i("onClick", view.toString());
        String button = String.valueOf(view.toString());


        Random rand = new Random();
        int randomValue = 0;

        // HitBtn pressed
        if (button.contains("hitBtn")) {
            randomValue = rand.nextInt(cards.length);
            this.p1Score += getValue(cards[randomValue]);
            Log.i("score", String.valueOf(this.p1Score));

            p1TextView[p1Counter++].setText(cards[randomValue]);
            Log.i("p1CardValue", cards[randomValue]);

        }

        if (p1Score > 21)
            Log.e("hit", "Limit Reached");


        // StayBtn pressed
        if (button.contains("stayBtn")) {
            randomValue = rand.nextInt(cards.length);
            p2Score += getValue(cards[randomValue]);
            Log.i("p2CardValue", String.valueOf(p2Score));

            p2TextView[p2Counter++].setText(cards[randomValue]);
            Log.i("p2CardValue", cards[randomValue]);

            if (p2Score > 21)
                Log.e("stay", "limit reached");

        }

        if (button.contains("foldBtn")) {
            finish();
        }


        if (p1Score <= 21 && p2Score > 21) {
            this.winnerView.setText("You WIN :) \n" + this.p1Score);
        } else if (p1Score > 21 && p2Score < 21) {
            this.winnerView.setText("You LOSE :(\n" + this.p1Score);
        }

    }

    public int getValue(String card) {
        switch (card) {
            case "J":
            case "Q":
            case "K":
                return 10;

            case "A":
                return 1;

            default:
                return Integer.parseInt(card);
        }
    }
}